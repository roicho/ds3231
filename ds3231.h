typedef struct
{
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hour;
	uint8_t day_week;
	char day_week_str[4];
	uint8_t day_month;
	uint8_t month;
	uint16_t year;
	char char_hour[9];
	char char_day[11];
} ds3231_T;

typedef enum
{
	DS3231_SECOND = 0,
	DS3231_MINUTE = 1,
	DS3231_HOUR = 2,
	DS3231_DAY_WEEK = 3,
	DS3231_DAY_MONTH = 4,
	DS3231_MONTH = 5,
	DS3231_YEAR = 6
}ds3231_reg_T;

void Ds3231Init(I2C_HandleTypeDef * i2c1);
void Ds3231GetTime(ds3231_T* time);
void Ds3231SetTime(ds3231_T time);
void Ds3231SetSingleReg(ds3231_reg_T reg, uint8_t value);
