
#include "stm32f1xx_hal.h"
#include "ds3231.h"

#define DS3231_I2C_ADDR_WRITE	(0xD0)
#define DS3231_I2C_ADDR_READ	(0xD1)

#define DS3231_MS_DATA		(0xF0)
#define DS3231_LS_DATA		(0x0F)
#define DS3231_SHIFT_4_BITS	(4U)
#define DS3231_BCD_FACTOR	(10U)
#define DS3231_ADDR_SECONDS		(0U)
#define DS3231_ADDR_MINUTES		(1U)
#define DS3231_ADDR_HOUR		(2U)
#define DS3231_ADDR_DAY_WEEK	(3U)
#define DS3231_ADDR_DAY_MONTH	(4U)
#define DS3231_ADDR_MONTH		(5U)
#define DS3231_ADDR_YEAR		(6U)
#define DS3231_TIME_SIZE		(7U)
#define DS3231_SINGLE_REG_SIZE	(2U)

#define DS3231_TIMEOUT		(1000U)

static I2C_HandleTypeDef * i2c1_p;

static uint8_t BcdToUint(uint8_t bcd);
static uint8_t UintToBcd(uint8_t uint);

static uint8_t BcdToUint(uint8_t bcd)
{
	uint8_t uint;

	uint = (((bcd & DS3231_MS_DATA) >> DS3231_SHIFT_4_BITS) * DS3231_BCD_FACTOR) + (bcd & DS3231_LS_DATA);

	return uint;
}

static uint8_t UintToBcd(uint8_t uint)
{
	uint8_t bcd;

	bcd = (((uint / DS3231_BCD_FACTOR) << DS3231_SHIFT_4_BITS) & DS3231_MS_DATA) +
			((uint % DS3231_BCD_FACTOR) & DS3231_LS_DATA);

	return bcd;
}

void Ds3231Init(I2C_HandleTypeDef * i2c1)
{
	i2c1_p = i2c1;
}

void Ds3231GetTime(ds3231_T* time)
{
	uint8_t data[DS3231_TIME_SIZE];
	uint8_t addr = DS3231_ADDR_SECONDS;
	uint8_t day_week_matrix[7][4] = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};
	uint8_t i = 0;

	HAL_I2C_Master_Transmit(i2c1_p, DS3231_I2C_ADDR_WRITE, &addr, sizeof(addr), DS3231_TIMEOUT);
	HAL_I2C_Master_Receive(i2c1_p, DS3231_I2C_ADDR_WRITE, data, sizeof(data), DS3231_TIMEOUT);

	time->seconds = BcdToUint(data[0]);
	time->minutes = BcdToUint(data[1]);
	time->hour = BcdToUint(data[2]);
	time->day_week = data[3];
	for(i = 0; i < 4; i ++)
	{
		time->day_week_str[i] = day_week_matrix[time->day_week - 1][i];
	}
	time->day_month = BcdToUint(data[4]);
	time->month = BcdToUint(data[5]);
	time->year = BcdToUint(data[6]);

	time->char_hour[0] = (time->hour / 10) + '0';
	time->char_hour[1] = (time->hour % 10) + '0';
	time->char_hour[2] = ':';
	time->char_hour[3] = (time->minutes / 10) + '0';
	time->char_hour[4] = (time->minutes % 10) + '0';
	time->char_hour[5] = ':';
	time->char_hour[6] = (time->seconds / 10) + '0';
	time->char_hour[7] = (time->seconds % 10) + '0';
	time->char_hour[8] = 0;

	time->char_day[0] = (time->day_month / 10) + '0';
	time->char_day[1] = (time->day_month % 10) + '0';
	time->char_day[2] = '/';
	time->char_day[3] = (time->month / 10) + '0';
	time->char_day[4] = (time->month % 10) + '0';
	time->char_day[5] = '/';
	time->char_day[6] = '2';
	time->char_day[7] = '0';
	time->char_day[8] = (time->year / 10) + '0';
	time->char_day[9] = (time->year % 10) + '0';
	time->char_day[10] = 0;
}

void Ds3231SetTime(ds3231_T time)
{
	uint8_t data[DS3231_TIME_SIZE + 1];

	data[0] = DS3231_ADDR_SECONDS;
	data[DS3231_ADDR_SECONDS + 1] = UintToBcd(time.seconds);
	data[DS3231_ADDR_MINUTES + 1] = UintToBcd(time.minutes);
	data[DS3231_ADDR_HOUR + 1] = UintToBcd(time.hour);
	data[DS3231_ADDR_DAY_WEEK + 1] = UintToBcd(time.day_week);
	data[DS3231_ADDR_DAY_MONTH + 1] = UintToBcd(time.day_month);
	data[DS3231_ADDR_MONTH + 1] = UintToBcd(time.month);
	data[DS3231_ADDR_YEAR + 1] = UintToBcd(time.year);

	HAL_I2C_Master_Transmit(i2c1_p, DS3231_I2C_ADDR_WRITE, data, sizeof(data), DS3231_TIMEOUT);
}


void Ds3231SetSingleReg(ds3231_reg_T reg, uint8_t value)
{
	uint8_t data[DS3231_SINGLE_REG_SIZE];

	data[0] = (uint8_t) reg;
	data[1] = UintToBcd(value);

	HAL_I2C_Master_Transmit(i2c1_p, DS3231_I2C_ADDR_WRITE, data, sizeof(data), DS3231_TIMEOUT);
}
